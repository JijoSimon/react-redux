import logo from "./logo.svg";
import "./App.css";
import App from "./components/App";
import configureStore from "./redux/store";
import { Provider } from "react-redux";

let initialState = {
  todos: localStorage.getItem("itemList")
    ? JSON.parse(localStorage.getItem("itemList"))
    : [],
};
let store = configureStore(initialState);

function AppInit() {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
}

export default AppInit;
