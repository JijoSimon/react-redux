import React, { Component } from 'react'
import TodoInput from './todoInput'
import TodoList from './todolist'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actions from '../redux/action'
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
class App extends Component {
  render() {
    return (
      <div>
        <Router>
        <Switch>
          <Route exact path="/" component={TodoInput} />
          <Route exact path="/home" component={TodoList} />
        </Switch>
      </Router>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return state
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App)