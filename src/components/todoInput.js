import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actions from '../redux/action'
class TodoInput extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: ''
    }
  }
  handleTitleChange(event) {
    this.setState({
      title: event.target.value
    })
  }
  async handleSubmit(event) {
    event.preventDefault()
    if (this.state.title != '') {
      let data = await this.props.actions.addTodo(this.state.title)
      console.log(data)
      //reset input box
      localStorage.setItem("itemList",JSON.stringify(this.props.todos))
      this.setState({
        title: ''
      })
    }
  }
  render() {
    console.log(this.props.todos)
    return (
      <div className="todo__input">
        <h1 className="header">Todos</h1>
        <div className="title">
          <input
            type="text"
            placeholder="title.."
            required={true}
            value={this.state.title}
            onChange={this.handleTitleChange.bind(this)}
          />
        </div>
        <div className="submitButton">
          <button onClick={this.handleSubmit.bind(this)}>Add</button>
        </div>
        <a href="\home">List</a>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return state
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoInput)