import React, { Component } from 'react'
import TodoItem from './todoitem'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actions from '../redux/action'

class TodoList extends Component {
  render() {
    console.log(this.props)
    return (
      <div>
               <h1 className="header">Todos</h1>
      <ul >
        {
          this.props.todos.map((todo) => {
            return <TodoItem key={todo.id} todo={todo} actions=
              {this.props.actions} />
          })
        }
      </ul>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return state
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (TodoList)