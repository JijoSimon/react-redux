import { applyMiddleware, compose, createStore } from 'redux'
import reducer from './reducer'
let finalCreateStore = compose(
  applyMiddleware()
)(createStore)
export default function configureStore(initialState = { todos: [] }) {
  return finalCreateStore(reducer, initialState)
}